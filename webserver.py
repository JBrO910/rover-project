import time
try:
	import usocket as socket
except:
	import socket

from DCmotor import DCMotor
from machine import Pin, PWM
import network

#Variables for Wheels
frequency = 15000
D1_PHA_A = Pin(17, Pin.OUT)
D1_EN_A = PWM(Pin(16), frequency)
D1_STBY = Pin(4, Pin.OUT)
D1_PHA_B = Pin(0, Pin.OUT)
D1_EN_B = PWM(Pin(15), frequency)

D2_EN_A = PWM(Pin(12), frequency)
D2_PHA_A = Pin(13, Pin.OUT)
D2_STBY = Pin(14, Pin.OUT)
D2_EN_B = PWM(Pin(26), frequency)
D2_PHA_B = Pin(27, Pin.OUT)

frontR = DCMotor(D1_STBY, D1_PHA_A, D1_EN_A, 350, 1023)
rearR = DCMotor(D1_STBY, D1_PHA_B, D1_EN_B, 350, 1023)

frontL = DCMotor(D2_STBY, D2_PHA_A, D2_EN_A, 350, 1023)
rearL = DCMotor(D2_STBY, D2_PHA_B, D2_EN_B, 350, 1023)

# Start with motors stopped
frontR.stop()
rearR.stop()
frontL.stop()
rearL.stop()

# Setup AP. Remember to change essid.
station = network.WLAN(network.AP_IF)
station.active(True)
station.config(essid = 'ESP32')
station.config(authmode=3, password = '12345678')

while station.isconnected() == False:
	pass

print('Connection successful')
print(station.ifconfig())

def web_page(request):
	motor_state = "Stopped"
	if request.find('/?forward') > 0:
		motor_state="Going Forward"
		frontR.forward(100)
		rearR.forward(100)
		frontL.forward(100)
		rearL.forward(100)
	if request.find('/?left') > 0:
		motor_state="Going Left"
		frontR.stop()
		rearR.stop()
		frontL.forward(100)
		rearL.forward(100)
	if request.find('/?right') > 0:
		motor_state="Going Right"
		frontL.stop()
		rearL.stop()
		frontR.forward(100)
		rearR.forward(100)
	if request.find('/?stop') > 0:
		motor_state="Stopped"
		frontR.stop()
		rearR.stop()
		frontL.stop()
		rearL.stop()

	html = '''<html><head> <title>Rover Web Server</title>
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <link rel="icon" href="data:,"> <style>
     html{font-family: Helvetica; display:inline-block; margin: 0px auto;
     text-align: center;}
     h1{color: #0F3376; padding: 2vh;}p{font-size: 1.5rem;}
     .button{display: inline-block; background-color: #e7bd3b; border: none;
     border-radius: 4px; color: white; text-decoration: none;
     font-size: 30px; width:100%}
     .button2{background-color: #4286f4; width:49%}
     </style></head>
     <body> <h1>Rover Web Server</h1>
     <p>Rover : <strong>""" + motor_state + """</strong></p>
     <p><a href='/?forward'><button class="button">Forward</button></a></p>
     <p><a href='/?left'><button class="button button2">LEFT</button></a>
     <a href='/?right'><button class="button button2" >RIGHT</button></a></p>
     <p><a href='/?stop'><button class="button button">STOP</button></a></p>
     </body></html>'''

	return html

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 80))
s.listen(5)

while True:
	conn, addr = s.accept()
	print('Got a connection from %s' % str(addr))
	request = conn.recv(1024)
	request = str(request)
	print('Content = %s' % request)
	response = web_page(request)
	conn.send(response)
	conn.close()