from DCmotor import DCMotor
from machine import Pin, PWM
from time import sleep
from hcsr04 import HCSR04

#Variables for Wheels
frequency = 15000
D1_PHA_A = Pin(17, Pin.OUT)
D1_EN_A = PWM(Pin(16), frequency)
D1_STBY = Pin(4, Pin.OUT)
D1_PHA_B = Pin(0, Pin.OUT)
D1_EN_B = PWM(Pin(15), frequency)

D2_EN_A = PWM(Pin(12), frequency)
D2_PHA_A = Pin(13, Pin.OUT)
D2_STBY = Pin(14, Pin.OUT)
D2_EN_B = PWM(Pin(26), frequency)
D2_PHA_B = Pin(27, Pin.OUT)

FrR = DCMotor(D1_STBY, D1_PHA_A, D1_EN_A, 350, 1023)
RrR = DCMotor(D1_STBY, D1_PHA_B, D1_EN_B, 350, 1023)

FrL = DCMotor(D2_STBY, D2_PHA_A, D2_EN_A, 350, 1023)
RrL = DCMotor(D2_STBY, D2_PHA_B, D2_EN_B, 350, 1023)

# Variables for Ultrasonic Sensor
#Right sensor
RightS = HCSR04(trigger_pin = 33, echo_pin = 25, echo_timeout_us = 10000)
#Left sensor
LeftS = HCSR04(trigger_pin = 5, echo_pin = 18, echo_timeout_us = 10000)


while True:
    distanceR = RightS.distance_cm()
    distanceL = LeftS.distance_cm()
    adjustRL = distanceR/distanceL
    adjustLR = distanceL/distanceR
    print("RIGHT: ", adjustRL)
    print("LEFT: ", adjustLR)
    if adjustRL > 1:
        #sleep(0.5)
        FrL.forward(100)
        RrL.forward(100)
        speedR = int(adjustLR*100)
        FrR.forward(speedR)
        RrR.forward(speedR)
    elif adjustLR < 1:
        #sleep(0.5)
        speedL = int(adjustRL*100)
        FrL.forward(speedL)
        RrL.forward(speedL)
        FrR.forward(100)
        RrR.forward(100)
    elif adjustLR >= 0:
        pass

        
















"""
#Left side wheels
pin1 = Pin(17, Pin.OUT)
pin2 = Pin(16, Pin.OUT)
pin3 = Pin(4, Pin.OUT)

#right side wheels
pin4 = Pin(13, Pin.OUT)
pin5 = Pin(26, Pin.OUT)
pin6 = Pin(12, Pin.OUT)
while True:
    pin1.on()
    pin2.on()
    pin3.on()
    pin4.on()
    pin5.on()
    pin6.on()
    sleep(2)
    pin1.off()
    pin2.off()
    pin3.off()
    pin4.off()
    pin5.off()
    pin6.off()
    sleep(5)
"""
