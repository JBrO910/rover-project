from hcsr04 import HCSR04
from time import sleep

sensor = HCSR04(trigger_pin = 5, echo_pin = 18, echo_timeout_us = 10000)
sensor2 = HCSR04(trigger_pin = 33, echo_pin = 25, echo_timeout_us = 10000)

def feedback(distance1, distance2):
    if distance1 > distance2:
        print('Turn right')
    elif distance1 < distance2:
        print('Turn left')

while True:
    distance = sensor.distance_cm()
    print('Sensor1 Distance:', distance, 'cm')
    distance2 = sensor2.distance_cm()
    print('Sensor2 Distance:', distance2, 'cm')
    
    feedback(distance, distance2)
    sleep(1)
    