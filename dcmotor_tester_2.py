from DCmotor import DCMotor
from machine import Pin, PWM
from time import sleep
from hcsr04 import HCSR04

#Variables for Wheels
frequency = 15000
D1_PHA_A = Pin(17, Pin.OUT)
D1_EN_A = PWM(Pin(16), frequency)
D1_STBY = Pin(4, Pin.OUT)
D1_PHA_B = Pin(0, Pin.OUT)
D1_EN_B = PWM(Pin(15), frequency)

D2_EN_A = PWM(Pin(12), frequency)
D2_PHA_A = Pin(13, Pin.OUT)
D2_STBY = Pin(14, Pin.OUT)
D2_EN_B = PWM(Pin(26), frequency)
D2_PHA_B = Pin(27, Pin.OUT)

frontR = DCMotor(D1_STBY, D1_PHA_A, D1_EN_A, 350, 1023)
rearR = DCMotor(D1_STBY, D1_PHA_B, D1_EN_B, 350, 1023)

frontL = DCMotor(D2_STBY, D2_PHA_A, D2_EN_A, 350, 1023)
rearL = DCMotor(D2_STBY, D2_PHA_B, D2_EN_B, 350, 1023)

# Variables for Ultrasonic Sensor
#Right sensor
sensorR = HCSR04(trigger_pin = 33, echo_pin = 25, echo_timeout_us = 10000)
#Left sensor
sensorL = HCSR04(trigger_pin = 5, echo_pin = 18, echo_timeout_us = 10000)


while True:
    distanceR = sensorR.distance_cm()
    distanceL = sensorL.distance_cm()
    print('R: ', distanceR)
    print('L: ', distanceL)
    sleep(1)
    if distanceR > distanceL:
        #sleep(0.5)
        print('RIGHT')
        frontL.backwards(100)
        rearL.backwards(100)
        frontR.forward(100)
        rearR.forward(100)
    elif distanceR < distanceL:
        #sleep(0.5)
        print('LEFT')
        frontL.forward(100)
        rearL.forward(100)
        frontR.forward(100)
        rearR.forward(100)
    elif distanceR < 0 or distanceL < 0:
        pass
    else:
        pass