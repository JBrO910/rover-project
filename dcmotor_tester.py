from dcmotor import DCMotor
from machine import Pin, PWM
from time import sleep

frequency = 15000
pin1 = Pin(17, Pin.OUT) # Replace x
pin2 = Pin(16, Pin.OUT) # Replace y
enable = PWM(Pin(4), frequency)
dc_motor = DCMotor(pin1, pin2, enable)
dc_motor = DCMotor(pin1, pin2, enable, 350, 1023)
print('Forward')
dc_motor.forward(50)
print('Sleep')
sleep(10)
print('Stop')
dc_motor.stop()
print('Sleep')
sleep(10)
print('Back')
dc_motor.backwards(100)
print('Sleep')
sleep(10)
print('Foward')
dc_motor.forward(60)
print('Sleep')
sleep(10)
print('Stop')
dc_motor.stop()
    